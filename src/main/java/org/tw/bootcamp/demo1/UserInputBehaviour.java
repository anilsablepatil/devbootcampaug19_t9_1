package org.tw.bootcamp.demo1;

import java.util.Scanner;

public class UserInputBehaviour implements MoveBehaviour {
    private Scanner sc;

    public UserInputBehaviour(Scanner sc) {
        this.sc = sc;
    }

    public PlayerMoves getMove() {
        String input = sc.next();
        return PlayerMoves.valueOf(input);
    }
}