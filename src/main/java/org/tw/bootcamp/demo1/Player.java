package org.tw.bootcamp.demo1;

public class Player {

    private MoveBehaviour moveBehaviour;

    private int totalScore = 0;

    private PlayerMoves recentMove;

    public Player(MoveBehaviour moveBehaviour) {
        this.moveBehaviour = moveBehaviour;
    }

    public PlayerMoves getMove() {
        return moveBehaviour.getMove();
    }

    public void addScore(int score) {
        this.totalScore += score;
    }

    public int getScore() {
        return totalScore;
    }

}
