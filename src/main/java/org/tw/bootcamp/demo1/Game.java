package org.tw.bootcamp.demo1;

import java.io.PrintStream;
import java.util.Observable;

public class Game extends Observable {
    private final Player p1;
    private final Player p2;
    private final Machine m;
    private int numberOfRounds;
    private PrintStream console;

    public Game(Player p1, Player p2, Machine m, int numberOfRounds, PrintStream console) {
        this.p1 = p1;
        this.p2 = p2;
        this.m = m;
        this.numberOfRounds = numberOfRounds;
        this.console = console;
    }

    public Game(Player p1, Player p2, Machine m, int numberOfRounds) {
        this.p1 = p1;
        this.p2 = p2;
        this.m = m;
        this.numberOfRounds = numberOfRounds;
    }

    public void play() {
        for(int i=0; i< this.numberOfRounds; i++) {
            PlayerMoves p1Move = this.p1.getMove();
            PlayerMoves p2Move = this.p2.getMove();
            ScoreForRound scoreForRound = m.calculateScore(p1Move, p2Move);
            this.p1.addScore(scoreForRound.getPlayerOneScore());
            this.p2.addScore(scoreForRound.getPlayerTwoScore());
            if(this.console != null)
                console.println("Round=" + (i + 1) + " P1=" + p1.getScore() + " P2=" + p2.getScore()) ;
        }
    }

    public void setValue(PlayerMoves move) {
        setChanged();
        notifyObservers(move);
    }

}
