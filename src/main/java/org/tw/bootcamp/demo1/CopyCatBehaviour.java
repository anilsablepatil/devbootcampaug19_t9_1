package org.tw.bootcamp.demo1;

import java.util.Observable;
import java.util.Observer;

public class CopyCatBehaviour implements MoveBehaviour, Observer {

    PlayerMoves otherPlayerMove = null;

    @Override
    public PlayerMoves getMove() {
        return otherPlayerMove == null ? PlayerMoves.COOPERATE : PlayerMoves.COOPERATE ;
    }

    @Override
    public void update(Observable o, Object arg) {
        this.otherPlayerMove = (PlayerMoves) arg;
    }
}
