package org.tw.bootcamp.demo1;

public class Machine {
    public ScoreForRound calculateScore(PlayerMoves playerOneMove, PlayerMoves playerTwoMove) {
        if (playerOneMove == PlayerMoves.COOPERATE && playerTwoMove == PlayerMoves.COOPERATE) {
            return new ScoreForRound(2, 2);
        }
        if (playerOneMove == PlayerMoves.COOPERATE && playerTwoMove == PlayerMoves.CHEAT) {
            return new ScoreForRound(-1, 3);
        }
        if (playerOneMove == PlayerMoves.CHEAT && playerTwoMove == PlayerMoves.COOPERATE) {
            return new ScoreForRound(3, -1);
        }
        return new ScoreForRound(0, 0);
    }
}
