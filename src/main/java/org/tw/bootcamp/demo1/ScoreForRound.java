package org.tw.bootcamp.demo1;

public class ScoreForRound {
    private int playerOneScore;
    private int playerTwoScore;

    public ScoreForRound(int playerOneScore, int playerTwoScore) {
        this.playerOneScore = playerOneScore;
        this.playerTwoScore = playerTwoScore;
    }

    public int getPlayerOneScore() {
        return playerOneScore;
    }

    public int getPlayerTwoScore() {
        return playerTwoScore;
    }
}
