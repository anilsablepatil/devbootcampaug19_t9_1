package org.tw.bootcamp.demo1;

public interface MoveBehaviour {
    public PlayerMoves getMove();
}
