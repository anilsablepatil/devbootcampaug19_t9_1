package org.tw.bootcamp.demo1;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Scanner;

import static org.mockito.Matchers.any;

public class PlayerTest {

    @Test
    public void shouldCreateObject() {
        Scanner sc = new Scanner("CHEAT");
        UserInputBehaviour userInputBehaviour = new UserInputBehaviour(sc);
        Player p1 = new Player(userInputBehaviour);
    }

    @Test
    public void shouldTakeInputAsCheat() {
        Scanner sc = new Scanner("CHEAT");
        UserInputBehaviour userInputBehaviour = new UserInputBehaviour(sc);
        Player p1 = new Player(userInputBehaviour);
        PlayerMoves move = p1.getMove();
        Assert.assertEquals(PlayerMoves.CHEAT, move);
    }

    @Test
    public void shouldTakeInputAsCooperate() {
        Scanner sc = new Scanner("COOPERATE");
        UserInputBehaviour userInputBehaviour = new UserInputBehaviour(sc);
        Player p1 = new Player(userInputBehaviour);
        PlayerMoves move = p1.getMove();
        Assert.assertEquals(PlayerMoves.COOPERATE, move);
    }
}
