package org.tw.bootcamp.demo1;

import org.junit.Assert;
import org.junit.Test;

public class CopyCatBehaviourTest {

    @Test
    public void shouldReturnOtherPlayerMove(){
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour();
        Assert.assertEquals(PlayerMoves.COOPERATE, copyCatBehaviour.getMove());
    }
}
