package org.tw.bootcamp.demo1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.PrintStream;
import java.util.Scanner;

public class GameIntegrationTest {

    @Test
    public void shouldReturnScoreForBothPlayer() {
        Scanner sc = new Scanner("CHEAT CHEAT");
        MoveBehaviour behaviour = new UserInputBehaviour(sc);
        Player player1 = new Player(behaviour);
        Player player2 = new Player(behaviour);
        Machine machine = new Machine();
        Game gameInstance = new Game(player1, player2, machine, 1);

        gameInstance.play();
        Assert.assertEquals(0, player1.getScore());
        Assert.assertEquals(0, player2.getScore());
    }

    @Test
    public void shouldReturnScoreForBothPlayerForTwoRounds() {
        Scanner sc = new Scanner("COOPERATE COOPERATE CHEAT COOPERATE");
        MoveBehaviour behaviour = new UserInputBehaviour(sc);
        Player player1 = new Player(behaviour);
        Player player2 = new Player(behaviour);
        Machine machine = new Machine();
        Game gameInstance = new Game(player1, player2, machine, 2);

        gameInstance.play();
        Assert.assertEquals(2 + 3, player1.getScore());
        Assert.assertEquals(2 + -1, player2.getScore());
    }

    @Test
    public void shouldReturnScoreForAlwaysCooperateAndCheatPlayer() {
        PrintStream console = Mockito.mock(PrintStream.class);
        Player cooperatePlayer = new Player(() -> PlayerMoves.COOPERATE);
        Player cheatPlayer = new Player(() -> PlayerMoves.CHEAT);
        Machine machine = new Machine();
        Game gameInstance = new Game(cooperatePlayer, cheatPlayer, machine, 5, console);

        gameInstance.play();
        Assert.assertEquals(-5, cooperatePlayer.getScore());
        Assert.assertEquals(15, cheatPlayer.getScore());

        Mockito.verify(console).println("Round=1 P1=-1 P2=3");
        Mockito.verify(console).println("Round=2 P1=-2 P2=6");
        Mockito.verify(console).println("Round=3 P1=-3 P2=9");
        Mockito.verify(console).println("Round=4 P1=-4 P2=12");
        Mockito.verify(console).println("Round=5 P1=-5 P2=15");
    }
}
