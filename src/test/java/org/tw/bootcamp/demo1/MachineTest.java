package org.tw.bootcamp.demo1;

import org.junit.Assert;
import org.junit.Test;

public class MachineTest {

    @Test
    public void shouldAbleToCreateInsanceOfMachine() {
        Machine m = new Machine();
    }

    @Test
    public void shouldGetScoreWhenBothCooperate() {
        Machine m = new Machine();
        ScoreForRound scores = m.calculateScore(PlayerMoves.COOPERATE, PlayerMoves.COOPERATE);
        Assert.assertEquals(2, scores.getPlayerOneScore());
        Assert.assertEquals(2,scores.getPlayerTwoScore());
    }

    @Test
    public void shouldGetScoresWhenBothCheat() {
        Machine m = new Machine();
        ScoreForRound scores = m.calculateScore(PlayerMoves.CHEAT, PlayerMoves.CHEAT);
        Assert.assertEquals(0, scores.getPlayerOneScore());
        Assert.assertEquals(0, scores.getPlayerTwoScore());
    }

    @Test
    public void shouldGetScoresWhenFirstPlayerCooperateAndSecondCheat() {
        Machine m = new Machine();
        ScoreForRound scores = m.calculateScore(PlayerMoves.COOPERATE, PlayerMoves.CHEAT);
        Assert.assertEquals(-1, scores.getPlayerOneScore());
        Assert.assertEquals(3, scores.getPlayerTwoScore());
    }

    @Test
    public void shouldGetScoresWhenFirstPlayerCheatAndSecondCooperate() {
        Machine m = new Machine();
        ScoreForRound scores = m.calculateScore(PlayerMoves.CHEAT, PlayerMoves.COOPERATE);
        Assert.assertEquals(3, scores.getPlayerOneScore());
        Assert.assertEquals(-1, scores.getPlayerTwoScore());
    }
}
