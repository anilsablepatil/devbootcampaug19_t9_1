package org.tw.bootcamp.demo1;




import org.junit.Before;
import org.junit.Test;

import org.mockito.Mockito;

import javax.swing.plaf.multi.MultiOptionPaneUI;

public class GameTest {
    Player player1;
    Player player2;
    Machine machine;
    Game game;

    @Before
    public void setUp() {
        player1 = Mockito.mock(Player.class);
        player2 = Mockito.mock(Player.class);
        machine = Mockito.mock(Machine.class);
        Mockito.when(machine.calculateScore(PlayerMoves.CHEAT, PlayerMoves.COOPERATE))
                .thenReturn(new ScoreForRound(3, -1));
        Mockito.when(machine.calculateScore(PlayerMoves.COOPERATE, PlayerMoves.CHEAT))
                .thenReturn(new ScoreForRound(-1, 3));
        Mockito.when(machine.calculateScore(PlayerMoves.CHEAT, PlayerMoves.CHEAT))
                .thenReturn(new ScoreForRound(0, 0));
        Mockito.when(machine.calculateScore(PlayerMoves.COOPERATE, PlayerMoves.COOPERATE))
                .thenReturn(new ScoreForRound(2, 2));
    }

    @Test
    public void shouldInvokeMoveAndCalculateScoreMethod() {
        game = new Game(player1, player2, machine, 1);
        Mockito.when(player1.getMove()).thenReturn(PlayerMoves.CHEAT);
        Mockito.when(player2.getMove()).thenReturn(PlayerMoves.COOPERATE);

        game.play();

        Mockito.verify(player1).getMove();
        Mockito.verify(player2).getMove();
        Mockito.verify(machine).calculateScore(PlayerMoves.CHEAT, PlayerMoves.COOPERATE);
    }

    @Test
    public void shouldHaveReturnScoreForPlayer1AndPlayer2() {
        game = new Game(player1, player2, machine, 1);
        Mockito.when(player1.getMove()).thenReturn(PlayerMoves.CHEAT);
        Mockito.when(player2.getMove()).thenReturn(PlayerMoves.COOPERATE);

        game.play();

        Mockito.verify(player1).addScore(3);
        Mockito.verify(player2).addScore(-1);
    }

    @Test
    public void shouldHaveReturnScoreForPlayer1AndPlayer2After3Rounds() {
        game = new Game(player1, player2, machine, 4);
        Mockito.when(player1.getMove())
                .thenReturn(PlayerMoves.CHEAT)
                .thenReturn(PlayerMoves.COOPERATE)
                .thenReturn(PlayerMoves.CHEAT)
                .thenReturn(PlayerMoves.COOPERATE);
        Mockito.when(player2.getMove())
                .thenReturn(PlayerMoves.COOPERATE)
                .thenReturn(PlayerMoves.CHEAT)
                .thenReturn(PlayerMoves.CHEAT)
                .thenReturn(PlayerMoves.COOPERATE);

        game.play();

        Mockito.verify(player1).addScore(3);
        Mockito.verify(player1).addScore(-1);
        Mockito.verify(player1).addScore(0);
        Mockito.verify(player1).addScore(2);

        Mockito.verify(player2).addScore(-1);
        Mockito.verify(player2).addScore(3);
        Mockito.verify(player2).addScore(0);
        Mockito.verify(player2).addScore(2);
    }

    @Test
    public void shouldSetState(){
        game = Mockito.mock(Game.class);
        game.setValue(PlayerMoves.CHEAT);
        Mockito.verify(game).setValue(PlayerMoves.CHEAT);

    }

    @Test
    public  void  shouldInvokeUpdate() {
        game = new Game(player1, player2, machine, 1);
        CopyCatBehaviour copyCatBehaviour = Mockito.mock(CopyCatBehaviour.class);
        game.addObserver(copyCatBehaviour);
        game.setValue(PlayerMoves.CHEAT);
        Mockito.verify(copyCatBehaviour).update(game,PlayerMoves.CHEAT);
    }


}
